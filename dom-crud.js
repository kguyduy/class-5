// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const cta = document.createElement('a');
cta.id = 'cta';
var text = document.createTextNode('Buy Now!');
cta.appendChild(text);
const main = document.getElementsByTagName('main')[0];
main.appendChild(cta);

// Access (read) the data-color attribute of the <img>,
// log to the console
const color = document.querySelector('[data-color]');
color.addEventListener('load', function(e) {
	console.log(e.target.dataset.color);
});


// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
var thirdItem = document.getElementsByTagName('li').item(2);
thirdItem.setAttribute('class', 'highlight');

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")

var el = document.querySelector('p');
el.remove();
