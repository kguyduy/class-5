// If an li element is clicked, toggle the class "done" on the <li>
const list = document.querySelectorAll('li');

const listToggle = function(e) {
	if (this.classList.contains('done')){
    this.classList.remove('done');
  } else {
    this.classList.add('done');
  }
};

for (let i = 0; i < list.length; i++) {
  list[i].addEventListener('click', listToggle)
};




// If a delete link is clicked, delete the li element / remove from the DOM

// const listItem = document.getElementsByTagName("li"); 

// const clickDelete = document.getElementsByClassName('delete')// or document.querySelectorAll("li"); 
// for (var i = 0; i < listItem.length; i++) {
//   listItem[i].onclick = function() {this.parentNode.removeChild(this);}
// }

let clickDelete = document.getElementsByClassName("delete");
Array.from(clickDelete).forEach(function(item) {
  // iterate and add the event handler to it
  item.addEventListener("click", function(e) {
    e.target.parentNode.remove();
    e.stopPropagation();
  });
});




// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const todayList = document.getElementsByClassName('today-list')[0]; 
const laterList = document.getElementsByClassName('later-list')[0]; 
const moveButton = document.querySelectorAll('a.move');

function moveTo() {
		if (this.classList.contains('toLater')) {
			this.classList.replace('toLater', 'toToday');
			this.innerHTML = 'Move to Today';
			laterList.appendChild(this.parentNode);
		} else if (this.classList.contains('toToday')) {
			this.classList.replace('toToday', 'toLater');
			this.innerHTML = 'Move to Later';
			todayList.appendChild(this.parentNode);
		} else {};
}
moveButton.forEach(function(moveButton) {
  this.addEventListener('click', moveTo);
});

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>
  input.value = '';
  const ul = this.parentNode.parentNode.getElementsByTagName('ul')[0];
  const li = document.createElement('li');
  const span = document.createElement('span');
  const newToDo = document.createTextNode(text);
  li.appendChild(span);
  span.appendChild(newToDo);
  li.firstChild.addEventListener('click', listToggle);
  ul.appendChild(li);

   // Move to later button
   const toLaterButton = document.createElement('a');
   toLaterButton.innerText = 'Move to Later'; 
   toLaterButton.classList.add('toLater');
   toLaterButton.classList.add('move');
   toLaterButton.addEventListener('click', moveTo);
   li.appendChild(toLaterButton);
 
 //   // Move to today button
   const toTodayButton = document.createElement('a');
   toTodayButton.innerText = 'Move to Today'; 
   toTodayButton.classList.add('toToday');
   toTodayButton.classList.add('move');
   toTodayButton.addEventListener('click', moveTo);
   li.appendChild(toTodayButton);
 
 // // Delete button
   const deleteButton = document.createElement('a');
   deleteButton.innerText = 'Delete'; 
   deleteButton.classList.add('delete');
   deleteButton.addEventListener('click', moveTo);
   li.appendChild(deleteButton);
   
}
let clickAdd = document.getElementsByClassName("add-item");
Array.from(clickAdd).forEach(function(item) {
  item.addEventListener("click", addListItem);
});

